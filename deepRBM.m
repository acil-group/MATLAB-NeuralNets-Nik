classdef deepRBM
    %DEEPRBM makes a deep network of RBM's
    
    properties
        shape
        nets
        n
    end
    
    methods
        function obj = deepRBM(s)
            obj.shape = s;
            obj.n = (length(s)-1);
            nets = cell(obj.n,1)
            for i=1:(length(s)-1)
                nets{i} = RBMnet(s(i:i+1));
                nets{i}.set_linear();
            end
            
        end
        
        function [Y, error] = learn(obj, X, LR, MR)
            y = X;
            avg_err = 0;
            for i=1:obj.n
                [y, err] = obj.nets{i}.learn(y, LR, MR);
                avg_err = avg_err+err;
            end
            if nargout == 2
                error = avg_err/obj.n;
            end
            Y = y;
        end
        
        function [Y, y] = feedForward(obj, X)
            Y = X;
            for i=1:obj.n
                if nargout == 2
                    [Y, y] = obj.nets{i}.feedForward(Y);
                else
                    [Y, y] = obj.nets{i}.feedForward(Y);
                end
            end
        end
        
        function Z = feedBackward(obj,Y, n)
            Z = Y;
            if nargin == 2
                n = -1;
                for i=2:length(obj.shape)
                    if obj.shape(i) == length(Z)
                        n = i-1;
                    end
                end
                if n < 0
                    error('Incorrect Vector Length')
                end
            end
            for i = n:-1:1
                Z = obj.nets{i}.feedBackward(Z);
            end
        end
        
        function Z = feedThrough(obj, X, n)
            Y = X;
            if nargin == 2
                n = obj.n;
            end
            for i=1:(n-1)
                Y = obj.nets{i}.feedForward(Y);
            end
            Z = obj.nets{n}.feedThrough(Y);
            for i=(n-1):-1:1
                Z = obj.nets{i}.feedBackward(Z);
            end
        end
        
        function delta = backProp(obj, error)
            delta = error;
            for i = obj.n:-1:1
                delta = obj.nets{i}.backProp(delta);
            end
        end
        
        function train(obj, X, LR, MR, e_goal)
            for i=1:obj.n
                if i ~= 1
                    s = [obj.nets{i-1}.shape{end}, length(X)];
                    X_new = zeros(s);
                    for j=1:length(X)
                        X_new(:,j) = obj.nets{i-1}.feedForward(X(:,i));
                    end
                    X = X_new;
                end
                obj.nets{i}.train(X, LR, MR, e_goal);
            end
        end
            
    end
    
end

