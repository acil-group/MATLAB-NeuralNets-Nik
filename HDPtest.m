%simple 1d HDP test
clc
clear all
close all
seed = randi(10000);
rng(seed)
dt = 0.1;
lambda = 0.9;
LR = 0.02;
MR = 0.0;
f = @(x) 1./(1+0.1*abs(x));
utility = @(x) x(1).^2 + x(3).^2;


A = HDPnet({[2,5,1],false,[3,10,1]}, utility, lambda);


j = 0;
num_solved = 0;

while num_solved < 500
    pos = 2*rand()-1;
    vel = 2*rand()-1;
    A.wait = true;
    
    for i=1:100
        if abs(pos) > 2 
            break
        end
        pm = tanh(pos);
        vm = tanh(vel);
        x = [pm;vm];
        [a,x_next,J] = A.feedForward(x);
        %a = a+0.01*(2*rand()-1);
        A.learn(LR,MR)
        vel = vel +a*dt;
        pos = pos+vel*dt;
    end
    j = j+i;
    pos = 2*rand()-1;
    vel = 2*rand()-1;
    A.wait = true;
    for i=1:100
        if abs(pos) > 2 
            break
        end
        pm = tanh(pos);
        vm = tanh(vel);
        x = [pm;vm];
        [a,x_next,J] = A.explore(x);
        %a = a+0.01*(2*rand()-1);
        A.passiveLearn(LR,MR)
        vel = vel +a*dt;
        pos = pos+vel*dt;
    end
    j = j+i;
    %disp([A.actor.norm_d_weights(),A.critic.norm_d_weights(),abs(pos)])
    i = 1;
    pos = 1;
    vel = 0;
    p = zeros(100,1);
    v = zeros(100,1);
    a_t = zeros(100,1);
    t = zeros(100,1);
    while abs(pos) > 0.01 || abs(vel) > 0.01
        if abs(pos) > 2 || i > 100
            break
        end
        p(i) = pos;
        v(i) = vel;
        t(i) = i;
        pm = tanh(pos);
        vm = tanh(vel);
        x = [pm;vm];
        a = A.actor.feedForward(x);
        a_t(i) = a;
        vel = vel +a*dt;
        pos = pos+vel*dt;
        i = i+1;
    end
    if ~(abs(pos) > 0.01 || abs(vel) > 0.01)
        %LR = LR*0.1;
        num_solved = num_solved +1;
        LR = LR/2;
    end
    
    if j > 1e5
        figure
        hold on
        t = t(1:i-1);
        p = p(1:i-1);
        v = v(1:i-1);
        a_t = a_t(1:i-1);
        plot(t,zeros(size(t)))
        plot(t,p,t,v,t,a_t);
        title('j100000 Control')
        xlabel('iterations')
        hold off
        j = 0;
    end
    fprintf('%d    %d\n',num_solved, j)
    
end

figure
t = t(1:i-1);
p = p(1:i-1);
v = v(1:i-1);
a_t = a_t(1:i-1);
hold on
grid on
plot(t,zeros(size(t)),'k-')
h1 = plot(t,p,'r-')
h2 = plot(t,v,'m-')
h3 = plot(t,a_t,'g-')
title('Final Control')
legend([h1, h2, h3],'Position','Velocity','Acceleration')
xlabel('iterations')
hold off



    

