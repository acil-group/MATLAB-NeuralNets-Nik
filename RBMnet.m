classdef RBMnet <handle
    %RBMNET Restricted Boltzman Machine
    
    properties
        net_A
        net_B
        shape
        weights
        d_weights
    end
    
    methods
        function obj = RBMnet(s)
            s = s(:);
            if length(s) ~= 2
                error('Shape Must be Size 2')
            end
            obj.shape = s(1:end);
            obj.net_A = nnet(obj.shape);
            obj.net_B = nnet(obj.shape(end:-1:1));
            
            obj.set_linear();
            
            obj.net_B.weights{1}(:,1:end-1) = obj.net_A.weights{1}(:,1:end-1)';
            obj.weights = cell(2,1);
            obj.d_weights = cell(2,1);
            obj.save_weights();
        end
        
        function set_linear(obj)
            active = {@(x) 1.*x; @(x) 1.*x};
            d_active = {@(x) 0.*x+1; @(x) 0.*x+1};
            obj.set_active(active, d_active)
        end
        
        function set_tanh(obj)
            active = {@(x) 1.*x; @(x) tanh(x)};
            d_active = {@(x) 0.*x+1; @(x) sech(x).^2};
            obj.set_active(active, d_active)
        end
        
        function set_active(obj, active, d_active)
            lin = @(x) 1.*x;
            d_lin = @(x) 0.*x + 1;
            
            obj.net_B.active{1} = active{2};
            obj.net_B.d_active{1} = d_active{2};
            obj.net_B.active{end} = lin;
            obj.net_B.d_active{end} = d_lin;

            obj.net_A.active{1} = active{1};
            obj.net_A.d_active{1} = d_active{1};
            obj.net_A.active{end} = active{1};
            obj.net_A.d_active{end} = d_active{1};
        end
        
        function load_weights_A(obj)
            obj.net_A.weights{1} = obj.weights{1};
            obj.net_A.d_weights{1} = obj.d_weights{1};
        end
        function load_weights_B(obj)
            obj.net_B.weights{1} = obj.weights{2};
            obj.net_B.d_weights{1} = obj.d_weights{2};
        end
        function load_weights(obj)
            obj.load_weights_A();
            obj.load_weights_B();
        end
        
        function save_weights_A(obj)
            obj.weights{1} = obj.net_A.weights{1};
            obj.d_weights{1} = obj.net_A.d_weights{1};
        end
        function save_weights_B(obj)
            obj.weights{2} = obj.net_B.weights{1};
            obj.d_weights{2} = obj.net_B.d_weights{1};
        end
        function save_weights(obj)
            obj.save_weights_A();
            obj.save_weights_B();
        end
        
        function swap_weights_A2B(obj)
            obj.net_B.weights{1}(:,1:end-1) = obj.net_A.weights{1}(:,1:end-1)';
            obj.net_B.d_weights{1}(:,1:end-1) = obj.net_A.d_weights{1}(:,1:end-1)';
        end
        
        function swap_weights_B2A(obj)
            obj.net_A.weights{1}(:,1:end-1) = obj.net_B.weights{1}(:,1:end-1)';
            obj.net_A.d_weights{1}(:,1:end-1) = obj.net_B.d_weights{1}(:,1:end-1)';
        end
        
        function [Y, error] = learn(obj, X, LR, MR)
            X = X(:);
            LR = LR/2;
            MR = MR/2;
            Y = obj.net_A.feedForward(X);
            Z = obj.net_B.feedForward(Y);
            
            lin_error = Z - X;
            delta = obj.net_B.backProp(lin_error, LR, MR);
            obj.swap_weights_B2A();
            obj.net_A.backProp(delta, LR, MR);
            obj.swap_weights_A2B();
            obj.save_weights();

            if nargout == 2
            	error = sum(lin_error.^2)/length(X);
            end
        end
        
        function Y = feedForward(obj, X)
            X = X(:);
            obj.load_weights_A();
            Y = obj.net_A.feedForward(X);
        end
        
        function Z = feedBackward(obj,Y)
            Y = Y(:);
            obj.load_weights_B();
            Z = obj.net_B.feedForward(Y);
        end
        
        function Z = feedThrough(obj, X)
            Y = obj.feedForward(X);
            Z = obj.feedBackward(Y);
        end
        
        function delta = backProp(obj, error)
            obj.load_weights_A();
            delta = obj.net_A.backProp(error);
        end
        
        function train(obj, X, LR, MR, e_goal)
            avg_err = 1;
            min_e = 100;
            min_weights = obj.weights;
            while avg_err > e_goal
                xi = randperm(length(X(1,:)));
                X = X(:,xi);
                avg_err = 0;
                for i=1:length(xi)
                    [Y, error] = obj.learn(X(:,i),LR,MR);
                    avg_err = avg_err + error;
                end
                avg_err = avg_err/length(xi);
                if avg_err < min_e
                    t = 0;
                    min_e = avg_err;
                    min_weights = obj.weights;
                else
                    t = t+1;
                end
                if t > 20
                    disp('Early Exit')
                    disp(min_e)
                    obj.weights = min_weights;
                    obj.load_weights();
                    break
                end
                fprintf('Error: %f, Min Error: %f, n: %d\n',avg_err, min_e, t)
            end
            disp('Trained')
        end
            
    end
    
end

