classdef ARTMAP <handle
    %Implementation of Fuzzy ARTMAP
    %Niklas Melton
    %09-03-2017
    properties
        A
        B
        map
    end
    
    methods
       function y = expand(obj, x, n)
            %-----expansion-----
            y = zeros(n,1);
            y(1:length(x)) = x;
            for i = (length(x)+1):n
                y(i) = y(i-1);
            end
        end
        
        function obj = ARTMAP(rho,alpha,beta)
            rho = obj.expand(rho,2);
            alpha = obj.expand(alpha,2);
            beta = obj.expand(beta,2);
            obj.A = ARTnet(rho(1),alpha(1),beta(1));
            obj.B = ARTnet(rho(2),alpha(2),beta(2));
            obj.map = zeros(0);
        end
        
        function obj = train(obj,x,y)
            rho_A = obj.A.rho;
            Ja = obj.A.train(x);
            Jb = obj.B.train(y);
            Jb_map = -1;
            %check for resonance
            while Jb_map ~= Jb
                if isempty(obj.map)
                    obj.map = zeros(Ja,Jb);
                    obj.map(Ja,Jb) = 1;
                    Jb_map = Jb;
                elseif any(size(obj.map)<[Ja,Jb])
                    new_map = zeros(Ja,Jb);
                    [n,m] = size(obj.map);
                    new_map(1:n,1:m) = obj.map;
                    obj.map = new_map;
                    obj.map(Ja,Jb) = 1;
                    Jb_map = Jb;
                else
                    [m,Jb_map] = max(obj.map(Ja,:));
                    if m == 0
                        obj.map(Ja,Jb) = 1;
                        Jb_map = Jb;
                    elseif Jb_map ~= Jb
                        %Resonant reset
                        I = obj.A.preprocess(x);
                        obj.A.rho = (obj.A.norm(min(I,obj.A.weights{Ja}))/obj.A.norm(I))+0.01;
                        Ja = obj.A.train(x);
                    end
                end
            end
            obj.A.rho = rho_A;
        end
        
        function [Jb,Ja] = run(obj,x)
            disp(x)
            Ja = obj.A.run(x);
            [m,Jb] = max(obj.map(Ja,:));
        end
            
    end
end

