%Nnet test
%Niklas Melton
%10-03-2017
clear all
close all
seed = randi(10000);
rng(seed);

LR = 0.01;
MR = 0.6;
e_tol = 0.001;

x = -1:0.01:1;
y = x.^2;%0.9*sin(x*pi);
A = nnet([1,4,1]);
A.nw_init();
%A.active{2} = @(x) 1./(1+exp(-x));

A.train(x,y,e_tol,LR,MR);
A.dcomplot(x,y)
%A.batchTrain(x,y,0.01,0.01,0.0,1000);