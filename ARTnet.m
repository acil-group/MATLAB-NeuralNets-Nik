classdef ARTnet   <handle
    %Implementation of Adaptive Resonance Theory
    %Niklas Melton
    %09-03-2017
    
    properties
        weights
        rho
        alpha
        beta
        active
        clusters
    end
    
    methods
        function obj = ARTnet(rho,alpha,beta)
            obj.weights = cell(1,1);
            obj.rho = rho;
            obj.alpha = alpha;
            obj.beta = beta;
            obj.active = @(x) 1*x;
            obj.clusters =0;
        end
        
        
        function n = norm(obj,x)
            n = sum(x);
        end
        
        function Ic = compliment(obj,I)
            Ic = [I;(1-I)];
        end
        
        function I = preprocess(obj,I)
            I = I(:);
            I = obj.active(I);
            I = obj.compliment(I);
            I = I./obj.norm(I);
        end
        
        function T = score(obj,I)
            T = zeros(obj.clusters,1);
            for i = 1:obj.clusters
                T(i) = obj.norm(min(I,obj.weights{i}))/(obj.alpha + obj.norm(obj.weights{i}));
            end
        end
        
        function R = resonance(obj,I,J)
            R = (obj.norm(min(I,obj.weights{J}))/obj.norm(I)) >= obj.rho;
        end
        
        function J = train(obj,I)
            I = obj.preprocess(I);
            if obj.clusters == 0
                obj.weights{1} = I;
                obj.clusters = obj.clusters +1;
            end

            T = obj.score(I);
            [Tmax,J] = max(T);
            %disp('----T1----')
            %disp(T)
            %disp([J,Tmax])
            while Tmax > 0
                if obj.resonance(I,J)
                    break
                else
                    T(J) = -1;
                    [Tmax,J] = max(T);
                end
            end
            if Tmax < 0
                J = obj.clusters+1;
                if J > length(obj.weights)
                    obj.weights = [obj.weights; cell(1,1)];
                end
                obj.weights{J} = I;
                obj.clusters = J;
            else
                obj.weights{J} = obj.beta*min(I,obj.weights{J})+(1-obj.beta)*obj.weights{J};
            end
        end
        
                
        function J = run(obj,I)
            if obj.clusters == 0
                J = 0;
                return
            end
            I = obj.preprocess(I);
            T = obj.score(I);
            [Tmax,J] = max(T);  
            while Tmax > 0
                if obj.resonance(I,J)
                    break
                else
                    T(J) = -1;
                    [Tmax,J] = max(T);
                end
            end
        end
        
        function [J, Tmax] = runUp(obj,I,n)
            if obj.clusters == 0
                error('No weights learned yet')
                exit()
            end
            I = obj.preprocess(I);
            T = obj.score(I);
            [Tmax,J] = max(T);
            %disp('----Tr----')
            %disp(T)
            %disp([J,Tmax])
            while Tmax > 0
                if obj.resonance(I,J)
                    break
                else
                    T(J) = -1;
                    [Tmax,J] = max(T);
                end
            end
            %pause
            for i=1:(n)
                Jo = J;
                T(J) = -1;
                [Tmax,J] = max(T);
                %isp('T')
                %disp(T)
                %disp('new J')
                %disp([J, Tmax])
                while Tmax > 0
                    if obj.resonance(I,J)
                        T(J) = -1;
                        Tmax = max(T);
                        break
                    else
                        T(J) = -1;
                        [Tmax,J] = max(T);
                    end
                end
                if Tmax < 0
                    %disp('broke')
                    break
                end
            end
        end            
    end
    
end

