classdef nnet <handle
    %Basic Neural Network class
    
    properties
        shape;
        weights;
        d_weights;
        field;
        nodes;
        active;
        d_active;
        grow
        prune
        td_trace
    end
    methods
        function w = nw_init(obj)
            for k = 1:length(obj.weights)
                ma = obj.shape(k);
                mb = obj.shape(k+1);
                Y = 0.7*(mb^(1/ma));
                for j = 1:mb
                    s = sqrt(mb*sum(obj.weights{k}(j,:).^2));
                    obj.weights{k}(j,:) = Y*obj.weights{k}(j,:)/s;
                end
            end
        end
                    
                    
        function obj = nnet(s)
            obj.shape = s;
            obj.nodes = cell(length(s),1);
            obj.field = cell(length(s),1);
            obj.weights = cell(length(s)-1,1);
            obj.d_weights = cell(length(s)-1,1);
            obj.active = cell(length(s),1);
            obj.d_active = cell(length(s),1);
            obj.td_trace = cell(length(obj.weights),1);
            %default activations
            active = @(x) tanh(x);
            d_active = @(x) sech(x).^2;
            obj.prune = true;
            
            for i=1:length(s)
                if s(i) == 0
                    s(i) = 1;
                    obj.grow = true;
                elseif isempty(obj.grow)
                    obj.grow = false;
                end
                obj.field{i} = zeros(s(i),1);
                if i~= 1
                    obj.active{i}= active;
                    obj.d_active{i} = d_active;
                else
                    obj.active{i} = @(x) 1*x;
                    obj.d_active{i} = @(x) 1+0*x;
                end
                if i ~= length(s)
                    %randomize weights
                    obj.weights{i} = (rand(s(i+1),s(i)+1)-0.5);
                    obj.d_weights{i} = zeros(size(obj.weights{i}));
                    %initialize td trace
                    obj.td_trace{i} = zeros(size(obj.weights{i}));
                    %add additional node for bias
                    obj.nodes{i} = zeros(s(i)+1,1);
                else
                    obj.nodes{i} = zeros(s(i),1);
                end
                
                
            end
           
        end
        
        function set_sigmoid(obj)
            active = @(x) 1./(1+exp(-x));
            d_active = @(x) active(x).*(1-active(x));
            for i=1:length(obj.shape)
                if i~= 1
                    obj.active{i}= active;
                    obj.d_active{i} = d_active;
                else
                    obj.active{i} = @(x) 1*x;
                    obj.d_active{i} = @(x) 1+0*x;
                end
            end
        end
        
        function set_linear(obj)
            for i=1:length(obj.shape)
                obj.active{i} = @(x) 1*x;
                obj.d_active{i} = @(x) 1+0*x;
            end
        end
        
        function set_input_activation(obj)
            obj.active{1}= obj.active{2};
            obj.d_active{1} = obj.d_active{2};
        end
        
        function dw = norm_d_weights(obj)
            dw = 0;
            for i=1:length(obj.weights)
                dw = dw+(sum(sum(abs(obj.d_weights{i})))/sum(size(obj.d_weights)));
            end
            dw = dw/length(obj.weights);
        end
                
        function Y = feedForward(obj,X)
            %feed input through net
            obj.field{1} = X;
            obj.nodes{1}(1:end-1) = obj.active{1}(obj.field{1});
            %keep biases high
            obj.nodes{1}(end) = 1;
            for i=1:(length(obj.shape)-1)
                if (i+1) ~= length(obj.shape)
                    %sum field
                    obj.field{i+1} = obj.weights{i}*obj.nodes{i};
                    %activations function
                    obj.nodes{i+1}(1:end-1) = obj.active{i+1}(obj.field{i+1});
                    %keep biases high
                    obj.nodes{i+1}(end) = 1;
                else
                    %sum field
                    obj.field{i+1} = obj.weights{i}*obj.nodes{i};
                    %activations function
                    obj.nodes{i+1} = obj.active{i+1}(obj.field{i+1});
                    %no biases in lower layer
                end
            end
            %return lowest layer
            Y = obj.nodes{end};
        end
        
        function prune_net(obj)
            for k = 1:(length(obj.weights)-1)
                f = abs(obj.weights{k})*ones(size(obj.nodes{k}));
                obj.weights{k} = obj.weights{k}(f>0.001,:);
                obj.d_weights{k} = obj.d_weights{k}(f>0.001,:);
                obj.nodes{k+1} = obj.nodes{k+1}(f>0.001);
                obj.field{k+1} = obj.nodes{k+1}(f>0.001);
            end
        end
        
        function grow_net(obj)
            for k = 1:(length(obj.weights)-1)
                
            end
        end
        
        function delta = backProp(obj,error,LR,MR)
            if nargin < 4
                MR = 0;
            end
            if nargin < 3
                LR = 0;
            end
                
            %basic backprop
            delta = error.*obj.d_active{end}(obj.field{end});
            for k = length(obj.shape)-1:-1:1
                %delta & chain rules & momentum
                if LR > 0.0
                    %weight update with momentum and decay
                    obj.d_weights{k} = MR*obj.d_weights{k} - (LR*delta*obj.nodes{k}');
                end
                delta = (obj.weights{k}(:,1:end-1)'*delta).*obj.d_active{k}(obj.field{k});
                if LR > 0.0
                    obj.weights{k} = obj.weights{k} + obj.d_weights{k};
                end
            end

        end
        
        function grad = gradient(obj)
            grad = cell(length(obj.shape),1);
            g_delta = obj.d_active{end}(obj.field{end});
            for i = length(obj.weights)
                grad(i) = g_delta*obj.nodes{k}';
                g_delta = (obj.weights{i}(:,1:end-1)'*delta).*obj.d_active{i}(obj.field{i});
            end
            grad(end) = g_delta;
        end
        
            
        function TD_learn(obj, error, LR, MR, lambda)
            grad = obj.gradient();
            for i =1:length(obj.weights)
                obj.trace{i} = grad{i}+lambda*obj.trace{i};
                if LR > 0
                    obj.d_weights{i} = MR*obj.d_weights{i} - (LR*error*obj.trace{i});
                end
            end    
        end
        
        function [d_weights,delta] = batchBackProp(obj,error,LR,MR)
            %batch backprop
            delta = error.*obj.d_active{end}(obj.field{end});
            for k = length(obj.weights):-1:1
                %delta & chain rules & momentum
                if LR > 0.0
                    %weight update with momentum and decay
                    obj.d_weights{k} = MR*obj.d_weights{k} - (1-MR)*(LR*delta*obj.nodes{k}');% - sign(obj.weights{k})*rand()*(1e-10);
                end
                delta = (obj.weights{k}(:,1:end-1)'*delta).*obj.d_active{k}(obj.field{k});
            end
            d_weights = obj.d_weights;
        end
        
        function batchUpdate(obj, d_weights)
            for k = length(obj.weights):-1:1
                obj.weights{k} = obj.weights{k} + d_weights{k};
            end
        end
        
        function batchTrain(obj,x,y,e_tol,LR,MR,bs)
            %trains until tolerance is achieved
            e = zeros(10,1);
            err_avg = 1;
            j = 1;
            while err_avg > e_tol
                batch_d_weights = zeros(0,0);
                for b = 1:bs
                    i = randi([1,length(x)]);
                    Yhat = obj.feedForward(x(:,i));
                    err_lin = (Yhat-y(:,i));
                    err_rel = err_lin./max(abs(y)')';
                    %keep a rolling error average
                    err_avg = err_avg*0.999 + 0.05*0.05*sum((err_rel).^2);
                    [d_weights,delta] = obj.batchBackProp(err_lin, LR, MR);
                    if isempty(batch_d_weights)
                       batch_d_weights = d_weights;
                    else
                        for n=1:length(d_weights)
                            batch_d_weights{n} = batch_d_weights{n} + d_weights{n};
                        end
                    end
                    e(j) = err_avg;
                    j = j+1;
                    if j > 0.9*length(e)
                       e = [e;zeros(length(e),1)];
                    end
                    %disp([err_avg])
                end
                obj.batchUpdate(batch_d_weights);
            end
            figure
            hold on
            title('Error')
            xlabel('iterations')
            ylabel('Error')
            comet(1:length(e),e)
            fprintf('Solution after %d iterations\n',j)         
        end
        
        function e = train2(obj,x,y,e_tol,LR,MR)
            %trains until tolerance is achieved
            e = zeros(10,1);
            err_avg = 1;
            j = 1;
            while err_avg > e_tol
               i = randi([1,length(x)]);
               Yhat = obj.feedForward(x(:,i));
               err_lin = (Yhat-y(:,i));
               err_rel = err_lin./max(abs(y)')';
               %keep a rolling error average
               err_avg = err_avg*0.999 + 0.001*sum((err_rel).^2);
               delta = obj.backProp(err_lin, LR, MR);
               e(j) = err_avg;
               j = j+1;
               if j > 0.9*length(e)
                   e = [e;zeros(length(e),1)];
               end
               disp([err_avg])
            end
            fprintf('Solution after %d iterations\n',j)         
        end
        
        function e = train(obj,x,y,e_tol,LR,MR)
            %train until tolerance is achieved
            e = zeros(10,1);
            j = 0;
            err_avg = 1;
            min_err = 1;
            min_weights = false;
            min_ct = 0;
            while err_avg > e_tol
                %shuffle training data
                k = randperm(length(x));
                x = x(:,k);
                y = y(:,k);
                x_train = x(:,1:floor(0.67*length(x)));
                y_train = y(:,1:floor(0.67*length(x)));
                x_test = x(:,ceil(0.67*length(x)):end);
                y_test = y(:,ceil(0.67*length(x)):end);

                for i=1:length(x_train)
                    Yhat = obj.feedForward(x_train(:,i));
                    err_lin = (Yhat - y_train(:,i));
                    err_rel = err_lin./max(abs(y)')';
                    %if err_rel > e_tol
                    delta = obj.backProp(err_lin, LR, MR);
                    %end
                end
                j = j+i;
                err_avg = 0;
                for i = 1:length(x_test)
                    Yhat = obj.feedForward(x(:,i));
                    err_lin = (Yhat - y(:,i));
                    err_rel = abs(err_lin)./max(abs(y)')';
                    %keep error average
                    err_avg = err_avg + err_rel;
                end
                err_avg = err_avg/length(x_test);
                ei = j/length(x_train);
                e(ei) = err_avg;
                if ei > 0.9*length(e)
                    e = [e;zeros(length(e),1)];
                end
                if err_avg < min_err
                    min_err = err_avg;
                    min_weights = obj.weights;
                    min_ct = 0;
                else
                    min_ct = min_ct+1;
                end
                if min_ct > 100
                    obj.weights = min_weights;
                    disp('error')
                    disp([min_err])
                    break
                end
                disp('error')
                disp([err_avg])
                
            end
            fprintf('Solution after %d iterations\n',j)         
        end
        
        function vtrain(obj,x,y,e_tol,LR,MR)
            e = obj.train(x,y,e_tol,LR,MR);
            figure
            hold on
            title('Error')
            xlabel('iterations')
            ylabel('Error')
            comet(1:length(e),e)
            complot(obj,x,y);
        end
        
        function obj = complot(obj,x,y)
            %Plots actual function vs approximations
            y_hat = zeros(size(y));
            %dx_hat = zeros(size(y));
            for i = 1:length(y_hat)
                y_hat(i) = obj.feedForward(x(i));
                %dx_hat(i) = obj.backProp(1,0,0);
            end
            figure
            hold on
            %dy = diff(y)./(pi*diff(x));
            %dx = linspace(min(x),max(x),length(dy));
            plot(x,y)
            plot(x,y_hat)
            %plot(dx,dy)
            %plot(x,dx_hat)
            legend('F(x)','F~(x)')%,'df(x)','dF~(x)')
            title('Comparison of Exact and Network Solutions')
            
        end
        function obj = dcomplot(obj,x,y)
            %Plots actual function vs approximations
            y_hat = zeros(size(y));
            dx_hat = zeros(size(y));
            for i = 1:length(y_hat)
                y_hat(i) = obj.feedForward(x(i));
                dx_hat(i) = obj.backProp(ones(size(obj.nodes{end})));
            end
            figure
            hold on
            dy = diff(y)./(pi*diff(x));
            dx = linspace(min(x),max(x),length(dy));
            plot(x,y)
            plot(x,y_hat)
            plot(dx,dy)
            plot(x,dx_hat)
            legend('F(x)','F~(x)','df(x)','dF~(x)')
            title('Comparison of Exact and Network Solutions')
            
        end        
        
        
        function dx = derive(obj, x)
            y = obj.feedForward(x);
            n = length(x);
            m = length(y);
            dx = zeros(n,m);
            for i=1:m
                dy = zeros(m,1);
                dy(i) = 1;
                dx(:,i) = obj.backProp(dy);
            end
        end
        
        function mse = MSE(obj,error)
            r = length(error);
            mse = sum(abs(error))/r;
        end
        
        function e = train_approx(obj,func,e_tol, LR, MR)
            n = obj.shape(1);
            m = obj.shape(end);
            o = 20*(2^(n+m));
            x = rand(n,o);
            y = zeros(m,o);
            for i=1:o
                y(:,i) = func(x(:,i));
            end
            e = obj.train(x,y,e_tol,LR,MR);
        end
    end
    
end

