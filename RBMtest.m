%test script for restricted boltzman machines
clc
clear all
close all

load 'tiger.mat'
net = RBMnet([64,16]);


LR = 0.01;
MR = 0.9;
e_tol = 0.0005;


x = zeros(0, 0);
m = 1;
for i=0:8:(length(tiger(1,:))-8)
    for j = 0:8:(length(tiger(:,1))-8)
        x(m,:) = reshape(tiger(j+1:j+8,i+1:i+8),[1,64]);
        m = m+1;
    end
end
norm_max = max(max(tiger));
x = x./norm_max;

x = x';
net.train(x, LR, MR, e_tol)


m = 1;
z = zeros(size(tiger));
for i=0:8:(length(z(1,:))-8)
    for j = 0:8:(length(z(:,1))-8)
        z_out = net.feedThrough(x(:,m));
        z(j+1:j+8,i+1:i+8) = reshape(z_out,[8,8]);
        m = m+1;
    end
end
z = z.*(norm_max./max(max(z)));

figure(1)
imshow(tiger, [0,100])
title('Origional')

figure(2)
imshow(z, [0,100])
title('Network Output')



figure(3)
d = abs(z-tiger);
m = max((max(d)));
imshow(d,[0,m]);
title('Difference')
