clc
clear all
close all

disp('RP')
LR = 0.1;
MR = 0.9;
rho = [0.9, 0.9];%, 0.5]
alpha = 0.01;
beta = 0.1;
gamma = 0.1;
rng(13);
A = RPnet([1,1],rho,alpha,beta,gamma);
B = nnet([1,3,1]);
%B.weights{1} = A.slp{1}.weights{1};
%B.weights{2} = A.slp{2}.weights{1};
B.set_sigmoid();
B.set_input_activation();
x = -1:0.01:1;
x = x*pi;
y = sin(x);

errors = zeros(1000, 2);
for i=1:1000
    d = randperm(length(x));
    x = x(d);
    y = y(d);
    A_e = 0;
    B_e = 0;
    for j = 1:length(x)
        Aout = A.learnForward(x(j));
        Bout = B.feedForward(x(j));
        A_error = Aout-(0.5*(y(j)+1));
        B_error = Bout-(0.5*(y(j)+1));
        A.backProp(A_error,LR,MR);
        B.backProp(B_error,LR,MR);
        A_e = A_e+abs(A_error);
        B_e = B_e+abs(B_error);
    end
    A_e = A_e/length(x);
    B_e = B_e/length(x);
    errors(i,1) = A_e;
    errors(i,2) = B_e;
    disp([A_e, B_e, i])
end
figure
hold on
title('Side-by-Side Training')
plot(1:1000,errors(:,1))
plot(1:1000,errors(:,2))
xlabel('Epochs')
ylabel('MSE')
legend('w/ Resonance', 'w/o Resonance')
grid on
hold off

%%
%XOR problem
clc
clear all
close all
x = [0,0,0,0; 0,0,0,1; 0,0,1,0; 0,0,1,1; 0,1,0,0; 0,1,0,1; 0,1,1,0;...
    0,1,1,1; 1,0,0,0; 1,0,0,1; 1,0,1,0; 1,0,1,1; 1,1,0,0; 1,1,0,1;...
    1,1,1,0; 1,1,1,1];
y = zeros(length(x(:,1)));
for i=1:length(x(:,1))
    if sum(x(i,:)) == 1
        y(i) = 1;
    end
end

LR = 1;
MR = 0.0;
rho = [0.95];
alpha = 0.1;
beta = 0.1;
gamma = 0.005;
A = RPnet([4,1],rho,alpha,beta,gamma);
j = 0;
err = 1;
while err > 0
    j = j+1;
    err = 0;
    for i = 1:length(x(:,1))
        Aout = A.learnForward(x(i,:));
        Aout = round(Aout);
        A_error = Aout-y(i);
        A.backProp(A_error,LR,MR);
        err = err + abs(A_error);
    end
end
disp(j-1)


%%
%Resonating RBM
clc
clear all
close all

load 'tiger.mat'
shape = [64,2];
net = RPnet(shape, rho, alpha, beta, gamma,{'RBM'});


LR = 0.01;
MR = 0.9;
e_tol = 0.0005;


x = zeros(0, 0);
m = 1;
for i=0:8:(length(tiger(1,:))-8)
    for j = 0:8:(length(tiger(:,1))-8)
        x(m,:) = reshape(tiger(j+1:j+8,i+1:i+8),[1,64]);
        m = m+1;
    end
end
norm_max = max(max(tiger));
x = x./norm_max;

x = x';
error = 1;
n = length(x(1,:));
while error > e_tol
    xi = randperm(n);
    error = 0;
    for i=1:n
        [y,err] = net.learnForward(x(:,xi(i)),LR,MR);
        error = error + err;
    end
    error = error/n;
    fprintf('Error: %f',error)
end


m = 1;
z = zeros(size(tiger));
for i=0:8:(length(z(1,:))-8)
    for j = 0:8:(length(z(:,1))-8)
        z_out = net.feedThrough(x(:,m));
        z(j+1:j+8,i+1:i+8) = reshape(z_out,[8,8]);
        m = m+1;
    end
end
z = z.*(norm_max./max(max(z)));

figure(1)
imshow(tiger, [0,100])
title('Origional')

figure(2)
imshow(z, [0,100])
title('Network Output')



figure(3)
d = abs(z-tiger);
m = max((max(d)));
imshow(d,[0,m]);
title('Difference')
