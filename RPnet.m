classdef RPnet <handle
    %First implementation of Resonating Perceptron Network
    %Niklas Melton
    %10-03-2017
    
    properties
        slp             %single layer perceptron networks
        weights         %slp weights
        d_weights       %slp d_weights
        art             %ART nets
        map             %maps for ARTMAPs
        gamma           %map growth rate
        active_clusters %keep track of clusters
        field          %keep track of neural activations
    end
    
    methods
        function y = expand(obj, x, n)
            %-----parameter expansion-----
            if iscell(x)
                for i = (length(x)+1):n
                    y = [y;y{i-1}];
                end
            else
                y = x(:);
                for i = (length(x)+1):n
                    y = [y;y(i-1)];
                end
            end
        end
        
        function obj = RPnet(shape, rho, alpha, beta, gamma, net_type)
            %create memory space
            n = length(shape)-1;
            obj.slp = cell(n,1);
            obj.weights = cell(n,1);
            obj.d_weights = cell(n,1);
            obj.art = cell(n+1,1);
            obj.map = cell(n,1);
            obj.active_clusters = zeros(n+1,1);
            obj.field = cell(n+1,1);
            %-----param expansion-----
            rho = obj.expand(rho,n+1);
            alpha = obj.expand(alpha,n+1);
            beta = obj.expand(beta,n+1);
            gamma = obj.expand(gamma,n);
            obj.gamma = gamma;
            if nargin == 5
                net_type = {'FF'};
            end
            net_type = expand(net_type,n);
            %-----network spawning-----
            for i = 1:n
                s = [shape(i),shape(i+1)];
                if strcmp(net_type{i},'RBM')
                    obj.slp{i} = RBMnet(s);
                    obj.slp{i}.set_linear();
                else
                    if ~strcmp(net_type{i},'FF')
                        fprintf('Setting Network %d to FF',i)
                    end
                    obj.slp{i} = nnet(s);
                    obj.slp{i}.set_linear();
                end
                obj.weights{i} = cell(1,1);
                obj.weights{i}{1} = obj.slp{i}.weights;
                obj.d_weights{i} = cell(1,1);
                obj.d_weights{i}{1} = obj.slp{i}.d_weights;
                obj.art{i} = ARTnet(rho(i),alpha(i),beta(i));
                obj.art{i}.active = @(x) 1*x;
                obj.map{i} = zeros(0);
            end
            obj.art{end} = ARTnet(rho(end),alpha(end),beta(end));
            obj.art{end}.active = @(x) 1*x;
        end
        
        function y = sigmoid(obj, x)
            y = 1./(1+exp(-x));
        end
        
        function dy = d_sigmoid(obj, x)
            dy = obj.sigmoid(x).*(1-obj.sigmoid(x));
        end
        
        function decrement(obj, i, Ja, Jb)
            n = length(obj.map{i}(Ja,:));
            jbb = [1:(Jb-1),(Jb+1):n];
            obj.map{i}(Ja,jbb) = obj.map{i}(Ja,jbb)-obj.gamma(i);
            obj.map{i}(Ja,:) = max(obj.map{i}(Ja,:),zeros(1,n));
        end
        
        function increment(obj, i, Ja, Jb)
            obj.map{i}(Ja,Jb) = obj.map{i}(Ja,Jb)+obj.gamma(i);
        end
        
        function prune_maps(obj)
            %for each map
            for i=1:length(obj.map)
                %disp([i,length(obj.map)])
                %for each row (or A cluster)
                j = 1;
                while j <= (length(obj.map{i}(:,1)))
                    jj = 0;
                    %disp([j,i])
                    %disp(obj.map{i})
                    %Check if this cluster is associated with any others
                    if all(obj.map{i}(j,:) <= 0.0)
                        disp('removed')
                        %pause
                        %remove cluster from art
                        L = [1:(j-1),(j+1):length(obj.art{i}.weights)];
                        obj.art{i}.weights = obj.art{i}.weights(L,:);
                        obj.art{i}.clusters = obj.art{i}.clusters -1;
                        %remove cluster from map
                        L = [1:(j-1),(j+1):length(obj.map{i}(1,:))];
                        obj.map{i} = obj.map{i}(L,:);
                        %remove weights
                        L = [1:(j-1),(j+1):length(obj.weights{i})];
                        obj.weights{i} = obj.weights{i}(L);
                        obj.d_weights{i} = obj.d_weights{i}(L);
                        if i ~= 1
                            %remove cluster from previous map
                            L = [1:(j-1),(j+1):length(obj.map{i-1}(:,1))];
                            obj.map{i} = obj.map{i-1}(:,L);
                        end
                    else
                        jj = 1;
                    end
                    %if mapping reaches 1, make permanent
                    [m,n] = max(obj.map{i}(j,:));
                    if m >= 1.0
                        obj.map{i}(j,:) = zeros(1,length(obj.map{i}(j,:)));
                        obj.map{i}(j,n) = 1.0;
                    end
                    j =j+jj;
                end
            end
            %check final artnet
            j = 1;
            %disp('map1')
            %disp(obj.map{end})
            while j <= (length(obj.map{i}(1,:)))
                if all(obj.map{end}(:,j) <= 0.0)
                    %disp('---V---')
                    %pause
                    %disp('weights')
                    %disp(size(obj.art{end}.weights))
                    %disp('map2')
                    %disp(obj.map{end})
                    %disp('-j-map')
                    %disp([j,(length(obj.map{i}(1,:)))])
                    %pause
                    %remove cluster from art
                    L = [1:(j-1),(j+1):length(obj.art{end}.weights)];
                    obj.art{end}.weights = obj.art{end}.weights(L,:);
                    obj.art{end}.clusters = obj.art{end}.clusters -1;
                    %remove cluster from map
                    L = [1:(j-1),(j+1):length(obj.map{end}(1,:))];
                    %disp('map 1')
                    %disp(size(obj.map{end}))
                    %disp('L')
                    %disp(L)
                    obj.map{end} = obj.map{end}(:,L);
                    %disp('map2')
                    %disp(size(obj.map{end}))
                    %remove weights
                    %L = [1:(j-1),(j+1):length(obj.weights{end})];
                    %obj.weights{end} = obj.weights{end}(L);
                    %disp('art end size 2')
                    %disp(size(obj.art{end}.weights))
                else
                    j = j+1;
                end
            end
            
            if length(obj.weights{1}) ~= obj.art{1}.clusters
                disp('pruned')
                disp(obj.weights{1})
                disp(obj.art{1}.weights)
                disp(obj.art{1}.clusters)
                %pause
            end
        end
        
            
        
        
        function [y, error] = learnForward(obj, x, LR, MR)
            %get initial cluster for 1st feature layer
            obj.field{1} = x;
            x = obj.sigmoid(x);
            Ja = obj.art{1}.train(x);
            obj.active_clusters(1,1) = Ja;
            if nargout == 2
                error = 0;
            end
            for i=1:length(obj.slp)
                Jb = 0;
                Jb_map = -1;
                
                %reuse clusters found in previous iter if possible
                rho_A = obj.art{i}.rho;
                while Jb_map ~= Jb
                    %expand slp weight memory as needed
                    n = length(obj.weights{i});
                    if Ja > n
                        obj.weights{i} = [obj.weights{i};cell(Ja-n,1)];
                        obj.d_weights{i} = [obj.d_weights{i};cell(Ja-n,1)];
                    end
                    if isempty(obj.weights{i}{Ja})
                        %use second best guess to fill weights in new
                        %cluster
                        %disp([Ja,i])
                        Ja2 = obj.art{i}.runUp(x,1);
                        if isempty(obj.weights{i}{Ja2})
                            disp(obj.weights{i})
                            disp(obj.map{i})
                            disp([Ja, Ja2, Jb, Jb_map])
                            error('Something went wrong...')
                        end
                        %copy weights from next best guess
                        obj.weights{i}{Ja} = obj.weights{i}{Ja2};
                        obj.d_weights{i}{Ja} = {zeros(size(obj.weights{i}{Ja2}{1}))};
                    end    
                    %set slp weights based on cluster Ja
                    obj.slp{i}.weights = obj.weights{i}{Ja};
                    obj.slp{i}.d_weights = obj.d_weights{i}{Ja};
                    %feed input through slp
                    if isa(obj.slp{i},'RBMnet')
                        [y, err] = obj.slp{i}.learn(x,LR,MR);
                    else 
                        y = obj.slp{i}.feedForward(x);
                    end
                        
                    obj.field{i+1} = y;
                    y = obj.sigmoid(y);
                    %cluster output of slp
                    Jb = obj.art{i+1}.train(y);
                    %check mapping
                    if isempty(obj.map{i})
                        %new map
                        obj.map{i} = zeros(Ja,Jb);
                        obj.map{i}(Ja,Jb) = obj.gamma(i);
                        Jb_map = Jb;
                    elseif any(size(obj.map{i}) < [Ja,Jb])
                        %new cluster, resize map
                        %disp('New Cluster')
                        %pause
                        new_map = zeros(Ja,Jb);
                        [n,m] = size(obj.map{i});
                        new_map(1:n,1:m) = obj.map{i};
                        obj.map{i} = new_map;
                        obj.map{i}(Ja,Jb) = obj.gamma(i);
                        Jb_map = Jb;
                    else
                        %check for resonance
                        [map_max,Jb_map] = max(obj.map{i}(Ja,:));
                        if map_max >= 1.0 && Jb_map ~= Jb
                            %resonant reset
                            obj.decrement(i,Ja,Jb);
                            disp('Resonant Reset')
                            %pause
                            if i ~= 1
                                %propogate reset backwards
                                Ja_old = obj.active_clusters(i-1);
                                obj.decrement(i-1,Ja_old,Ja);
                                obj.increment(i-1,Ja_old,Ja);
                            end
                            I = obj.art{i}.preprocess(x);
                            %adjust rho value
                            obj.art{i}.rho = (obj.art{i}.norm(min(I,obj.art{i}.weights{Ja}))/obj.art{i}.norm(I))+0.01;
                            %rerun for new cluster
                            %disp('reran')
                            Ja = obj.art{i}.train(x);
                            %disp([Ja,i])
                        elseif map_max < 1.0
                            %map too uncertain to reset
                            %increment selected cluster mapping
                            obj.increment(i,Ja,Jb);
                            %decrement all other associated mappings
                            obj.decrement(i,Ja,Jb);
                            Jb_map = Jb;
                        end
                    end
                end
                %return rho to origional value
                obj.art{i}.rho = rho_A;
                
                %keep track of which clusters were used
                obj.active_clusters(i) = Ja;
                obj.active_clusters(i+1) = Jb;
                %carry over cluster and nodal values
                Ja = Jb;
                x = y;
                if nargout == 2
                    error = error+err;
                end
            end
            if nargout == 2
                n = 0;
                for i=1:length(obj.slp)
                    if isa(obj.slp{i},'RBMnet')
                        n = n+1;
                    end
                end
                error = error/n;
            end
            obj.prune_maps();
        end
        
        function y = feedForward(obj, x)
            %get initial cluster for 1st feature layer
            obj.field{1} = x;
            x = obj.sigmoid(x);
            Ja = obj.art{1}.run(x);
            obj.active_clusters(1,1) = Ja;
            %set slp weights based on cluster Ja
            obj.slp{1}.weights = obj.weights{1}{Ja};
            %feed input through slp
            y = obj.slp{1}.feedForward(x);
            obj.field{2} = y;
            y = obj.sigmoid(y);
            for i=1:(length(obj.map)-1)
                %check mapping
                [map_max,Jb_map] = max(obj.map{i}(Ja,:));
                %set slp weights based on mapping
                obj.slp{i+1}.weights = obj.weights{i+1}{Jb_map};
                %feed input through slp
                y = obj.slp{i+1}.feedForward(y);
                obj.field{i+2} = y;
                y = obj.sigmoid(y);
                %carry over cluster and nodal values
                Ja = Jb_map;
            end
        end
        
        function feedThrough(
        
        function delta = backProp(obj,error,LR,MR)
            n = length(obj.slp);
            LR = obj.expand(LR,n);
            MR = obj.expand(MR,n);
            delta = error.*obj.d_sigmoid(obj.field{end});
            for i = n:-1:1
                j = obj.active_clusters(i);
                %disp(obj.active_clusters')
                %disp(j)
                obj.slp{i}.weights = obj.weights{i}{j};
                obj.slp{i}.d_weights = obj.d_weights{i}{j};
                if isa(obj.slp{i},'RBMnet')
                    delta = obj.slp{i}.backProp(delta).*obj.d_sigmoid(obj.field{i});
                else
                    delta = obj.slp{i}.backProp(delta,LR(i),MR(i)).*obj.d_sigmoid(obj.field{i});
                end
                obj.weights{i}{j} = obj.slp{i}.weights;
                obj.d_weights{i}{j} = obj.slp{i}.d_weights;
            end
            %pause
            %obj.slp{1}.weights{1}
            %obj.slp{2}.weights{1}
            %pause
        end
        
        function e = train(obj,x,y,e_tol,LR,MR)
            %train until tolerance is achieved
            e = zeros(10,1);
            j = 0;
            err_avg = 1;
            while err_avg > e_tol
                %shuffle training data
                k = randperm(length(x));
                x = x(:,k);
                y = y(:,k);
                x_train = x(:,1:floor(0.67*length(x)));
                y_train = y(:,1:floor(0.67*length(x)));
                x_test = x(:,ceil(0.67*length(x)):end);
                y_test = y(:,ceil(0.67*length(x)):end);

                for i=1:length(x_train)
                    Yhat = obj.learnForward(x_train(:,i));
                    err_lin = (Yhat - y_train(:,i));
                    err_rel = err_lin./max(abs(y)')';
                    %disp('err_rel')
                    %disp(err_rel)
                    %if err_rel > e_tol
                    delta = obj.backProp(err_lin, LR, MR);
                    %end
                end
                j = j+i;
                err_avg = 0;
                for i = 1:length(x_test)
                    Yhat = obj.feedForward(x(:,i));
                    err_lin = (Yhat - y(:,i));
                    err_rel = abs(err_lin)./max(abs(y)')';
                    %keep error average
                    err_avg = err_avg + err_rel;
                end
                err_avg = err_avg/length(x_test);
                ei = j/length(x_train);
                e(ei) = err_avg;
                if ei > 0.9*length(e)
                    e = [e;zeros(length(e),1)];
                end
                disp('error')
                disp([err_avg, ei])
                
            end
            fprintf('Solution after %d iterations\n',j)         
        end
        
        function vtrain(obj,x,y,e_tol,LR,MR)
            e = obj.train(x,y,e_tol,LR,MR);
            figure
            hold on
            title('Error')
            xlabel('iterations')
            ylabel('Error')
            comet(1:length(e),e)
            complot(obj,x,y);
        end
   
        function obj = complot(obj,x,y)
            %Plots actual function vs approximations
            y_hat = zeros(size(y));
            %dx_hat = zeros(size(y));

            for i = 1:length(y_hat)
                y_hat(i) = obj.feedForward(x(i));
                %dx_hat(i) = obj.backProp(1,0,0);
            end
            figure
            hold on
            %dy = diff(y)./(pi*diff(x));
            %dx = linspace(min(x),max(x),length(dy));
            plot(x,y,'-k')
            plot(x,y_hat)
            %plot(dx,dy)
            %plot(x,dx_hat)
            legend('F(x)','F~(x)')%,'df(x)','dF~(x)')
            title('Comparison of Exact and Network Solutions')
            
            figure
            hold on
            plot(x,y,'-k')
            for i=1:length(obj.weights{1})
                
                %for j = 1:length(obj.weights{2})
                    y_hat = zeros(size(y));
                    obj.slp{1}.weights = obj.weights{1}{i};
                    if length(obj.slp) == 2
                        obj.slp{2}.weights = obj.weights{2}{j};
                    end
                    for k = 1:length(y_hat)
                        %disp(length(obj.slp))
                        %disp(length(x))
                        %disp(length(y))
                        %disp([k,i])
                        xx = obj.slp{1}.feedForward(x(k));
                        if length(obj.slp) == 2
                            y_hat(k) = obj.slp{2}.feedForward(xx);
                        else
                            y_hat(k) = xx;
                        end
                    end
                    plot(x,y_hat)
                %end
            end
            title('Full Overlay')            
            
        end
    end
    
end

