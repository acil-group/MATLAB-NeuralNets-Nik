classdef DHPnet <handle
    %DHPnet maintains a DHP network form control
    
    properties
        actor
        model
        d_model
        approx_model
        critic
        approx_utility
        utility
        d_utility
        gamma
        nodes_last
        fields_last
        wait
    end
    
    methods
        
        function obj = DHPnet(shape, model, utility, gamma)
            obj.actor = nnet(shape{1});
            obj.critic = nnet(shape{3});
            obj.critic.active{end} = @(x) x;
            obj.critic.d_active{end} = @(x) 1+0*x;
            obj.gamma = gamma;
            obj.nodes_last{1} = false;
            obj.fields_last{1} = false;
            obj.wait = 0;
            
            if iscell(utility)
                obj.set_utility(utility{1},utility{2});
            else
                obj.approx_utility = nnet(shape{4});
                obj.approx_utility.active{end} = @(x) x;
                obj.approx_utility.d_active{end} = @(x) 1+0*x;
                obj.approx_utility.train_approx(utility,0.005,0.001,0.6);
                obj.set_utility(@(x) obj.approx_utility.feedForward(x),@(x) obj.approx_utility.derive(x))
            end
            if iscell(model)
                obj.set_model(model{1},model{2});
                obj.approx_model = false;
            else
                obj.approx_model = nnet(shape{2});
                obj.approx_model.train_approx(model,0.005,0.001,0.6);
                obj.set_model(@(x) obj.approx_model.feedForward(x),@(x) obj.approx_model.derive(x));
            end
            
        end
        
        function set_utility(obj, utility, d_utility)
            obj.utility = utility;
            obj.d_utility = d_utility;
        end
        function set_model(obj, model, d_model)
            obj.model = model;
            obj.d_model = d_model;
        end
        
        
        
        function [a,x_next,G] = feedForward(obj, x)
            obj.nodes_last{1} = obj.actor.nodes;
            obj.fields_last{1} = obj.actor.field;
            a = obj.actor.feedForward(x);
            if nargout > 1
                xa = [x;a];
                if obj.approx_model ~= false
                    obj.nodes_last{2} = obj.approx_model.nodes;
                    obj.fields_last{2} = obj.approx_model.field;
                end
                x_next = obj.model(xa);
                obj.nodes_last{3} = obj.critic.nodes;
                obj.fields_last{3} = obj.critic.field;
                G = obj.critic.feedForward(x_next);
            end
        end
        
      
        function learn(obj,LR,MR, R)
            if ~obj.wait
                if length(LR) < 3
                    LR = LR*ones(3,1);
                end
                if length(MR) < 3
                    MR = MR*ones(3,1);
                end

                nodes{1} = obj.actor.nodes;
                fields{1} = obj.actor.field;
                obj.actor.nodes = obj.nodes_last{1};
                obj.actor.field = obj.fields_last{1};
                if obj.approx_model ~= false
                    nodes{2} = obj.approx_model.nodes;
                    fields{2} = obj.approx_model.field;
                    obj.approx_model.nodes = obj.nodes_last{2};
                    obj.approx_model.field = obj.fields_last{2};
                end
                nodes{3} = obj.critic.nodes;
                fields{3} = obj.critic.field;
                obj.critic.nodes = obj.nodes_last{3};
                obj.critic.field = obj.fields_last{3};
                i = length(obj.actor.nodes{end});
                %u = obj.utility(obj.critic.field{1});
                du = obj.d_utility(obj.critic.field{1});
                if nargin == 4
                    G_err = obj.critic.nodes{end}-(R);
                else
                    G_err = obj.critic.nodes{end}-(du+obj.gamma*nodes{3}{end});
                end
                if G_err < 0.1
                    xa = [obj.actor.field{1};obj.actor.nodes{end}];
                    dm = obj.d_model(xa);
                    g = obj.critic.nodes{end};
                    dmg = dm*g;
                    dmg = dmg(end-i+1:end);
                    du = du(end-i+1:end);
                    bp_actor = du + obj.gamma*dmg;
                    obj.actor.backProp(bp_actor,LR(1),MR(1));
                end
                obj.critic.backProp(G_err,LR(3),MR(3));
                
                if obj.approx_model ~= false
                    model_err = obj.approx_model.nodes{end}-nodes{2}{1}(1:i);
                    obj.approx_model.backProp(model_err,LR(2),MR(2));
                end
                
                obj.actor.nodes = nodes{1};
                obj.actor.field = fields{1};
                if obj.approx_model ~= false
                    obj.approx_model.nodes = nodes{2};
                    obj.approx_model.field = fields{2};
                end
                obj.critic.nodes = nodes{3};
                obj.critic.field = fields{3};
                %disp([obj.actor.field{1}(1),obj.actor.nodes{end},da,obj.actor.norm_d_weights(),G_err])
            else
                obj.wait =false;
            end
            
        end
        
        function [a,x_next,G] = explore(obj, x)
            obj.nodes_last{1} = obj.actor.nodes;
            obj.fields_last{1} = obj.actor.field;
            a_n = obj.actor.feedForward(x);
            a = obj.actor.active{end}(10*(rand(length(obj.actor.field{end}),1)-0.5));
            xa = [x;a];
            if obj.approx_model ~= false
                obj.nodes_last{2} = obj.approx_model.nodes;
                obj.fields_last{2} = obj.approx_model.field;
                obj.approx_model.feedForward(xa);
            end
            x_next = obj.model(xa);
            obj.nodes_last{3} = obj.critic.nodes;
            obj.fields_last{3} = obj.critic.field;
            G = obj.critic.feedForward(x_next);
        end
        
        
        function passiveLearn(obj,LR,MR, R)
            if ~obj.wait
                if length(LR) == 1
                    LR = LR*ones(3,1);
                end
                if length(MR) == 1
                    MR = MR*ones(3,1);
                end

                nodes{1} = obj.actor.nodes;
                fields{1} = obj.actor.field;
                obj.actor.nodes = obj.nodes_last{1};
                obj.actor.field = obj.fields_last{1};
                if obj.approx_model ~= false
                    nodes{2} = obj.approx_model.nodes;
                    fields{2} = obj.approx_model.field;
                    obj.approx_model.nodes = obj.nodes_last{2};
                    obj.approx_model.field = obj.fields_last{2};
                end
                nodes{3} = obj.critic.nodes;
                fields{3} = obj.critic.field;
                obj.critic.nodes = obj.nodes_last{3};
                obj.critic.field = obj.fields_last{3};
                i = length(obj.actor.nodes{end});
                u = obj.utility(obj.critic.field{1});
                du = obj.d_utility(obj.critic.field{1});
                
                if nargin == 4
                    G_err = obj.critic.nodes{end}-(R);
                else
                    G_err = obj.critic.nodes{end}-(du+obj.gamma*nodes{3}{end});
                end
                if G_err < 0.1
                    xa = [obj.actor.field{1};obj.actor.nodes{end}];
                    dm = obj.d_model(xa);
                    g = obj.critic.nodes{end};
                    dmg = dm*g;
                    dmg = dmg(end-i+1:end);
                    duu = du(end-i+1:end);
                    bp_actor = duu + obj.gamma*dmg;
                    obj.actor.backProp(bp_actor,LR(1),MR(1));
                end
                
                obj.critic.backProp(G_err,LR(3),MR(3));
                if obj.approx_model ~= false
                    model_err = obj.approx_model.nodes{end}-nodes{2}{1}(1:i);
                    obj.approx_model.backProp(model_err,LR(2),MR(2));
                    obj.approx_model.nodes = nodes{2};
                    obj.approx_model.field = fields{2};
                end

                obj.actor.nodes = nodes{1};
                obj.actor.field = fields{1};
                
                obj.critic.nodes = nodes{3};
                obj.critic.field = fields{3};
                %disp([obj.actor.field{1}(1),obj.actor.nodes{end},da,obj.actor.norm_d_weights(),G_err])
            else
                obj.wait =false;
            end
            
        end
           
        function a = learnForward(obj,x,LR,MR)
            if length(LR) == 1
                LR = LR*ones(3,1);
            end
            if length(MR) == 1
                MR = MR*ones(3,1);
            end

            [a,x_next,G_next] = obj.feedForward(x);
            
            i = length(obj.actor.nodes{end});
            xa = [x;a];
            dm = obj.d_model(xa);
            dmg = dm*G_next;
            dmgu = dmg(end-i+1:end);
            dmg = dmg(1:end-i);
            du = obj.d_utility(xa);
            duu = du(end-i+1:end);
            du = du(1:end-i);
            dG = du+obj.gamma*dmg;
            bp_actor = duu + obj.gamma*dmgu;
            obj.actor.backProp(bp_actor,LR(1),MR(1));
           
            G = obj.critic.feedForward(x);
            G_err = G - (dG);
            obj.critic.backProp(G_err,LR(3),MR(3));
            if obj.approx_model ~= false
                nodes = obj.approx_model.nodes;
                field = obj.approx_model.field;
                obj.approx_model.nodes = obj.nodes_last{2};
                obj.approx_model.field = obj.fields_last{2};
                model_err = obj.approx_model.nodes{end}-obj.actor.field{1};
                obj.approx_model.backProp(model_err,LR(2),MR(2));
                obj.approx_model.nodes = nodes;
                obj.approx_model.field = field;
            end
            
        end
           
    end
    
end

