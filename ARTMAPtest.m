%ARTnet basic test
clc
clear all
close all

rho = 0.8;
alpha = 0.3;
beta = 0.9;

n1 = 100;
n2 = 100;
n3 = 100;

net = ARTMAP(rho,alpha,beta);
c1 = [0.2*rand(n1,2)+[0.6,0.8], zeros(n1,1)];
c2 = [0.3*rand(n2,2)+[0.1,0.2], 0.5*ones(n2,1)];
c3 = [0.2*rand(n3,2)+[0.5,0.5], ones(n3,1)];

c1a = c1(1:floor(n1/2),:);
c1b = c1(ceil(n1/2):end,:);
c2a = c2(1:floor(n2/2),:);
c2b = c2(ceil(n2/2):end,:);
c3a = c3(1:floor(n3/2),:);
c3b = c3(ceil(n3/2):end,:);

da = [c1a;c2a;c3a];
da = da(randperm(length(da)),:);
db = [c1b;c2b;c3b];
db = db(randperm(length(db)),:);

figure
hold on
for i=1:length(da)
    if da(i,3) == 0
        plot(da(i,1),da(i,2),'ro');
    elseif da(i,3) == 0.5
        plot(da(i,1),da(i,2),'bo');
    else
        plot(da(i,1),da(i,2),'go');
    end
end
title('Raw Data')
hold off


for j=1:3*length(da)
    i = mod((j-1),length(da))+1;
    net.train(da(i,1:2),da(i,3));
end
figure
for i=1:length(db)
    hold on
    J = net.run(db(i,1:2));
    if J == 1
        plot(db(i,1),db(i,2),'ro')
    elseif J == 2
        plot(db(i,1),db(i,2),'bo')
    elseif J == 3
        plot(db(i,1),db(i,2),'go')
    else
        plot(db(i,1),db(i,2),'yo')
    end
end

figure
hold on
for x=0:0.01:1
    for y = 0:0.01:1
        [J,j] = net.run([x,y]);
        if J == 1
            plot(x,y,'ro')
        elseif J == 2
            plot(x,y,'bo')
        elseif J == 3
            plot(x,y,'go')
        else
            plot(x,y,'yo')
        end
    end
end