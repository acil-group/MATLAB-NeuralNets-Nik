classdef HDPnet <handle
    %HDPnet operates an HDP network for control
    
    properties
        actor
        model
        critic
        utility
        lambda
        nodes_last
        fields_last
        wait
    end
    
    methods
        
        function obj = HDPnet(shape, utility, lambda)
            obj.actor = nnet(shape{1});
            if shape{2} ~= false
                obj.model = nnet(shape{2});
            else
                obj.model = false;
            end
            obj.critic = nnet(shape{3});
            obj.critic.active{end} = @(x) x;
            obj.critic.d_active{end} = @(x) 1+0*x;
            obj.lambda = lambda;
            obj.utility = utility;
            obj.nodes_last{1} = false;
            obj.fields_last{1} = false;
            obj.wait = 0;
        end
        
        function [a,x_next,J] = feedForward(obj, x)
            obj.nodes_last{1} = obj.actor.nodes;
            obj.fields_last{1} = obj.actor.field;
            a = obj.actor.feedForward(x);
            xa = [x;a];
            if obj.model ~= false
                obj.nodes_last{2} = obj.model.nodes;
                obj.fields_last{2} = obj.model.field;
                x_next = obj.model.feedForward(xa);
                obj.nodes_last{3} = obj.critic.nodes;
                obj.fields_last{3} = obj.critic.field;
                J = obj.critic.feedForward(x_next);
            else
                obj.nodes_last{2} = [];
                obj.fields_last{2} = [];
                x_next = [];
                obj.nodes_last{3} = obj.critic.nodes;
                obj.fields_last{3} = obj.critic.field;
                J = obj.critic.feedForward(xa);
            end
            
        end
        

        
        function learn(obj,LR,MR, R)
            if ~obj.wait
                if length(LR) == 1
                    LR = LR*ones(3,1);
                end
                if length(MR) == 1
                    MR = MR*ones(3,1);
                end

                nodes{1} = obj.actor.nodes;
                fields{1} = obj.actor.field;
                obj.actor.nodes = obj.nodes_last{1};
                obj.actor.field = obj.fields_last{1};
                if obj.model ~= false
                    nodes{2} = obj.model.nodes;
                    fields{2} = obj.model.field;
                    obj.model.nodes = obj.nodes_last{2};
                    obj.model.field = obj.fields_last{2};
                else
                    nodes{2} = [];
                    fields{2} = [];
                end
                nodes{3} = obj.critic.nodes;
                fields{3} = obj.critic.field;
                obj.critic.nodes = obj.nodes_last{3};
                obj.critic.field = obj.fields_last{3};
                i = length(obj.actor.nodes{end});
                if obj.model ~= false
                    u = obj.utility(obj.model.field{1});
                else
                    u = obj.utility(obj.critic.field{1});
                end
                if nargin == 4
                    J_err = obj.critic.nodes{end}-(R);
                else
                    J_err = obj.critic.nodes{end}-(u+obj.lambda*nodes{3}{end});
                end
                if J_err < 0.05
                    dx_next = obj.critic.backProp(1,0,0);
                    if obj.model ~= false
                        dxa = obj.model.backProp(dx_next,0,0);
                    else
                        dxa = dx_next;
                    end

                    
                    da = dxa(end-i+1:end);
                    dx = obj.actor.backProp(da,LR(1),MR(1));
                end
                obj.critic.backProp(J_err,LR(3),MR(3));
                
                if obj.model ~= false
                    model_err = obj.model.nodes{end}-nodes{2}{1}(1:i);
                    obj.model.backProp(model_err,LR(2),MR(2));
                end

                obj.actor.nodes = nodes{1};
                obj.actor.field = fields{1};
                if obj.model ~= false
                    obj.model.nodes = nodes{2};
                    obj.model.field = fields{2};
                end
                obj.critic.nodes = nodes{3};
                obj.critic.field = fields{3};
                %disp([obj.actor.field{1}(1),obj.actor.nodes{end},da,obj.actor.norm_d_weights(),J_err])
            else
                obj.wait =false;
            end
            
        end
        
            
        
        function [a,x_next,J] = explore(obj, x)
            obj.nodes_last{1} = obj.actor.nodes;
            obj.fields_last{1} = obj.actor.field;
            a_n = obj.actor.feedForward(x);
            a = obj.actor.active{end}(10*(rand(length(obj.actor.field{end}),1)-0.5));
            xa = [x;a];
            if obj.model ~= false
                obj.nodes_last{2} = obj.model.nodes;
                obj.fields_last{2} = obj.model.field;
                x_next = obj.model.feedForward(xa);
                obj.nodes_last{3} = obj.critic.nodes;
                obj.fields_last{3} = obj.critic.field;
                J = obj.critic.feedForward(x_next);
            else
                obj.nodes_last{2} = [];
                obj.fields_last{2} = [];
                x_next = [];
                obj.nodes_last{3} = obj.critic.nodes;
                obj.fields_last{3} = obj.critic.field;
                J = obj.critic.feedForward(xa);
            end
            
        end
        
        
        function passiveLearn(obj,LR,MR, R)
            if ~obj.wait
                if length(LR) == 1
                    LR = LR*ones(3,1);
                end
                if length(MR) == 1
                    MR = MR*ones(3,1);
                end

                nodes{1} = obj.actor.nodes;
                fields{1} = obj.actor.field;
                obj.actor.nodes = obj.nodes_last{1};
                obj.actor.field = obj.fields_last{1};
                if obj.model ~= false
                    nodes{2} = obj.model.nodes;
                    fields{2} = obj.model.field;
                    obj.model.nodes = obj.nodes_last{2};
                    obj.model.field = obj.fields_last{2};
                else
                    nodes{2} = [];
                    fields{2} = [];
                end
                nodes{3} = obj.critic.nodes;
                fields{3} = obj.critic.field;
                obj.critic.nodes = obj.nodes_last{3};
                obj.critic.field = obj.fields_last{3};
                i = length(obj.actor.nodes{end});
                if obj.model ~= false
                    u = obj.utility(obj.model.field{1});
                else
                    u = obj.utility(obj.critic.field{1});
                end
                if nargin == 4
                    J_err = obj.critic.nodes{end}-(R);
                else
                    J_err = obj.critic.nodes{end}-(u+obj.lambda*nodes{3}{end});
                end
                obj.critic.backProp(J_err,LR(3),MR(3));
                
                if obj.model ~= false
                    model_err = obj.model.nodes{end}-nodes{2}{1}(1:i);
                    obj.model.backProp(model_err,LR(2),MR(2));
                end
                %disp([size(obj.actor.field{1}),size(obj.actor.nodes{end})])
                xa = [obj.actor.field{1};obj.actor.nodes{end}];
                if obj.model ~= false
                    x_next = obj.model.feedForward(xa);
                else
                    x_next = xa;
                end
                J = obj.critic.feedForward(x_next);
                
                if J_err < 0.05
                    dx_next = obj.critic.backProp(1,0,0);
                    if obj.model ~= false
                        dxa = obj.model.backProp(dx_next,0,0);
                    else
                        dxa = dx_next;
                    end 
                    da = dxa(end-i+1:end);
                    %disp([obj.actor.nodes{end},da])
                    dx = obj.actor.backProp(da,LR(1),MR(1));
                end

                obj.actor.nodes = nodes{1};
                obj.actor.field = fields{1};
                if obj.model ~= false
                    obj.model.nodes = nodes{2};
                    obj.model.field = fields{2};
                end
                obj.critic.nodes = nodes{3};
                obj.critic.field = fields{3};
                %disp([obj.actor.field{1}(1),obj.actor.nodes{end},da,obj.actor.norm_d_weights(),J_err])
            else
                obj.wait =false;
            end
            
        end
           
        function a = learnForward(obj,x,LR,MR)
            if obj.model == false
                error('Cannot learn ADHDP forward')
                exit()
            end
            if length(LR) == 1
                LR = LR*ones(3,1);
            end
            if length(MR) == 1
                MR = MR*ones(3,1);
            end

            [a,x_next,J_next] = obj.feedForward(x);
            
            dx_next = obj.critic.backProp(-1,0,0);
            dxa = obj.model.backProp(dx_next,0,0);
            i = length(obj.actor.nodes{end});
            da = dxa(end-i+1:end);
            obj.actor.backProp(da,LR(1),MR(1));
            
            u = obj.utility(obj.actor.field{1});
            J = obj.critic.feedForward(obj.actor.field{1});
            J_err = J - (u+obj.lambda*J_next);
            obj.critic.backProp(J_err,LR(3),MR(3));
            if obj.nodes_last{1} ~= false
                nodes = obj.model.nodes;
                field = obj.model.field;
                obj.model.nodes = obj.nodes_last{2};
                obj.model.field = obj.fields_last{2};
                model_err = obj.model.nodes{end}-obj.actor.field{1};
                obj.model.backProp(model_err,LR(2),MR(2));
            end
            obj.model.nodes = nodes;
            obj.model.field = field;
            
        end
           
    end
    
end

