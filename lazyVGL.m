classdef lazyVGL
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        actor
        model
        critic
        utility
        approx_utility
        lambda
        gamma
        E
        wait
    end
    
    methods
        
        function u_net = approx_utility(utility,shape)
            u_net = nnet(shape);
            m = 50*(n^2);
            x = rand(n,m);
            y = zeros(1,m);
            for i=1:m
                y(i) = utility(x(:,i));
            end
            u_net.train(x,y,0.01,0.01,0.5);
        end
        
            
        function obj = lazyVGL(shape,utility,gamma,lambda)
            obj.shape = shape;
            obj.utility = utility;
            obj.gamma = gamma;
            obj.lambda = lambda;
            obj.approx_utility = approx_utility(utility,shape{end});
            obj.actor = nnet(shape{1});
            obj.model = nnet(shape{2});
            obj.critic = nnet(shape{3});
            obj.wait = 0;
        end
        
        function vec = weights2vec_c(obj)
            n = prod(size(obj.critic.weights));
            vec = reshape(obj.critic.weights,[n,1]);
        end
        
        function vec2weights_c(obj, vec)
            s = size(obj.critic.weights);
            obj.critic.weights = reshape(vec, s);
        end
            
    end
    
end

